"""Looker tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_looker.streams import QueriesStream, QueryDataStream,LooksStream , LooksDataStream

STREAM_TYPES = [
    QueriesStream,
    QueryDataStream,
    LooksStream,
    LooksDataStream,
]


class TapLooker(Tap):
    """Looker tap class."""

    name = "tap-looker"
    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("subdomain", th.StringType, required=True),
        th.Property("query_ids", th.StringType, required=True),
        th.Property("user_agent", th.StringType),
        th.Property("remove_limit_param", th.BooleanType,default=False)
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapLooker.cli()
