"""Stream type classes for tap-looker."""


from typing import Any, Dict, Iterable, Optional

import requests
from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_looker.client import LookerStream


class QueriesStream(LookerStream):
    """Define custom stream."""

    name = "queries"
    path = "/queries"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.CustomType({"type": ["number", "string"]})),
        th.Property("view", th.StringType),
        # th.Property("name", th.StringType),
        th.Property("slug", th.StringType),
    ).to_dict()

    def get_records(self, context: Optional[dict]) -> Iterable[Dict[str, Any]]:
        records = []
        query_ids = self.config.get("query_ids", None)
        if query_ids is not None:
            query_ids = query_ids.split(",")
            for query in query_ids:
                if query:
                    res = self.get_query_details(query)
                    res_status_code = res.status_code
                    res = res.json()
                    if res_status_code == 404:
                        res = self.get_query_details(query, "SQL").json()
                        query_type = "sql_queries"
                        res["id"] = res["slug"]
                        res["view"] = f"SQL-Query-{res['id']}"
                    else:
                        # make url for non SQL queries
                        query_type = "queries"

                    records.append(
                        {
                            "id": res["id"],
                            "slug": query,
                            "view": res["view"],
                            "query_type": query_type,
                            "total": res.get('total'),
                            'has_table_calculations':res.get("has_table_calculations")
                        }
                    )

        return records

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "slug": record["slug"],
            "query_id": record["id"],
            "query_type": record["query_type"],
            "total":record.get('total'),
            "has_table_calculations":record.get('has_table_calculations'),
        }


class QueryDataStream(LookerStream):
    """Define custom stream."""

    name = "query_data"
    path = "/{query_type}/{query_id}/run/json"
    primary_keys = ["query_id"]
    parent_stream_type = QueriesStream
    schema = th.PropertiesList(
        th.Property("query_id", th.CustomType({"type": ["number", "string"]})),
        th.Property("slug", th.StringType),
        th.Property("data", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        data = response.json()
        #Don't have access to context lets use partitions
        if self.partitions:
            query = self.partitions[-1]
            if isinstance(query,dict):
                if query.get('total'):
                    #Query's Total is set to true. Filter out the last record. 
                    data = data[:-1]
        for record in data:
            return_row = {}
            return_row["data"] = record
            yield return_row
class LooksStream(LookerStream):
    """Define custom stream."""

    name = "looks"
    path = "/looks"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.CustomType({"type": ["number", "string"]})),
        th.Property("view", th.StringType),
        th.Property("title", th.StringType),
        # th.Property("name", th.StringType),
        th.Property("slug", th.StringType),
    ).to_dict()

    def get_records(self, context: Optional[dict]) -> Iterable[Dict[str, Any]]:
        records = []
        look_ids = self.config.get("look_ids", None)
        if look_ids is not None:
            look_ids = look_ids.split(",")
            for look in look_ids:
                if look:
                    res = self.get_look_details(look)
                    res_status_code = res.status_code
                    res = res.json()
                    records.append(
                        {
                            "id": res["id"],
                            "public_slug": res.get("public_slug"),
                            "title": res.get("title"),
                            "query_id": res.get("query_id"),
                            "total": res.get('total'),
                            'has_table_calculations':res.get("has_table_calculations")
                        }
                    )

        return records

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "slug": record["public_slug"],
            "look_id": record["id"],
            "query_id": record["query_id"],
            "total":record.get('total'),
            "has_table_calculations":record.get('has_table_calculations'),
        }


class LooksDataStream(LookerStream):
    """Define custom stream."""

    name = "looks_data"
    path = "/looks/{look_id}/run/json"
    primary_keys = ["look_id"]
    parent_stream_type = LooksStream
    schema = th.PropertiesList(
        th.Property("look_id", th.CustomType({"type": ["number", "string"]})),
        th.Property("slug", th.StringType),
        th.Property("data", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        data = response.json()
        #Don't have access to context lets use partitions
        if self.partitions:
            query = self.partitions[-1]
            if isinstance(query,dict):
                if query.get('total'):
                    #Query's Total is set to true. Filter out the last record. 
                    data = data[:-1]
        for record in data:
            return_row = {}
            # partition = self.partitions[-1]
            # if partition:
            #     return_row['look_id'] = partition.get("look_id")
            #     return_row['slug'] = partition.get("slug")
            return_row["data"] = record
            yield return_row
