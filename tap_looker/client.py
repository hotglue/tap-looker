"""REST client handling, including LookerStream base class."""

from datetime import datetime, timedelta
from typing import Any, Dict, Optional, cast, Callable, Union

import backoff
import requests
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from singer_sdk.exceptions import RetriableAPIError
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
import backoff
import logging

API_DOMAIN_DEFAULT = "looker.com"
API_PORT_DEFAULT = "19999"
API_VERSIONS_SUPPORTED = ["2.99", "3.0", "3.1", "4.0"]
API_VERSION_DEFAULT = "4.0"


class Server5xxError(Exception):
    pass


class Server429Error(Exception):
    pass


class LookerStream(RESTStream):
    """Looker stream class."""

    _access_token = None
    _expires_in = None
    active_query = None
    queries_custom_limit = []
    override_prepared_request = False

    # _page_size = 10
    # _page_number = 1
    @property
    def url_base(self) -> str:
        url = "https://{}.{}:{}/api/{}".format(
            self.config.get("subdomain"),
            self.config.get("domain", API_DOMAIN_DEFAULT),
            self.config.get("port", API_PORT_DEFAULT),
            self.config.get("api_version", API_VERSION_DEFAULT),
        )
        return url

    records_jsonpath = "$[*]"  # Or override `parse_response`.
    next_page_token_jsonpath = "$.next_page"  # Or override `get_next_page_token`.

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        headers["Accept"] = "application/json"
        default_agent = "tap-looker ({}.{})".format(
            self.config.get("subdomain"), self.config.get("domain", API_DOMAIN_DEFAULT)
        )

        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent", default_agent)

        headers["Authorization"] = f"token {self.get_access_token()}"
        return headers

    # API Authentication:
    #  https://docs.looker.com/reference/api-and-integration/api-reference/v3.1/api-auth
    @backoff.on_exception(backoff.expo, Server5xxError, max_tries=5, factor=2)
    def get_access_token(self):
        if self._access_token is not None and self._expires_in > datetime.utcnow():
            return self._access_token

        headers = {}
        headers["Accept"] = "application/json"
        default_agent = "tap-looker ({}.{})".format(
            self.config.get("subdomain"), self.config.get("domain", API_DOMAIN_DEFAULT)
        )

        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent", default_agent)

        headers = headers

        response = requests.post(
            url="{}/login".format(self.url_base),
            headers=headers,
            data={
                "client_id": self.config.get("client_id"),
                "client_secret": self.config.get("client_secret"),
            },
        )

        if response.status_code >= 500:
            raise Server5xxError()

        if response.status_code != 200:
            looker_response = response.json()
            looker_response.update({"status": response.status_code})
            raise Exception(
                "Unable to authenticate (Looker response: `{}`)".format(looker_response)
            )

        data = response.json()
        self._access_token = data["access_token"]
        expires_seconds = data["expires_in"] - 60  # pad by 60 seconds
        self._expires_in = datetime.utcnow() + timedelta(seconds=expires_seconds)
        return self._access_token

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        if self.next_page_token_jsonpath:
            all_matches = extract_jsonpath(
                self.next_page_token_jsonpath, response.json()
            )
            first_match = next(iter(all_matches), None)
            next_page_token = first_match
        else:
            next_page_token = response.headers.get("X-Next-Page", None)

        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key

        if not context.get("has_table_calculations"):
            # params['limit'] = self._page_size
            if not self.config.get("remove_limit_param"):
                params["limit"] = -1

        if "query_type" in context:
            if context.get("query_type") == "sql_queries":
                # Receive data as a stream
                params["download"] = True
        if "query_id" in context:
            if context.get("query_id") in self.queries_custom_limit:
                # For now lets set it to 30000
                params["limit"] = 30000
        return params

    def get_query_details(self, query, query_type="query"):
        if query_type == "query":
            url = f"{self.url_base}/queries/slug/{query}"
        elif query_type == "SQL":
            url = f"{self.url_base}/sql_queries/{query}"
        return requests.get(url=url, headers=self.http_headers)

    def get_look_details(self, look_id):
        url = f"{self.url_base}/looks/{look_id}"
        return requests.get(url=url, headers=self.http_headers)

    def prepare_request(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> requests.PreparedRequest:
        # Reset method to GET
        self.rest_method = "GET"
        # SQL queries need POST request to run.
        if "query_type" in context:
            if context.get("query_type") == "sql_queries":
                self.rest_method = "POST"

        if "query_id" in context:
            self.active_query = context.get("query_id")

        http_method = self.rest_method
        url: str = self.get_url(context)
        params: dict = self.get_url_params(context, next_page_token)
        request_data = self.prepare_request_payload(context, next_page_token)
        headers = self.http_headers

        authenticator = self.authenticator
        if authenticator:
            headers.update(authenticator.auth_headers or {})
            params.update(authenticator.auth_params or {})

        request = cast(
            requests.PreparedRequest,
            self.requests_session.prepare_request(
                requests.Request(
                    method=http_method,
                    url=url,
                    params=params,
                    headers=headers,
                    json=request_data,
                ),
            ),
        )
        return request

    def request_decorator(self, func: Callable) -> Callable:
        """Instantiate a decorator for handling request failures."""
        decorator: Callable = backoff.on_exception(
            self.backoff_wait_generator,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError,
            ),
            max_tries=self.backoff_max_tries,
            on_backoff=self.backoff_handler,
        )(func)
        return decorator

    def validate_response(self, response: requests.Response) -> None:
        if (
            response.status_code in self.extra_retry_statuses
            or 500 <= response.status_code < 600
        ):
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)
        elif 400 <= response.status_code < 500:
            self.logger.warn(response.text)
            self.queries_custom_limit.append(self.active_query)
            msg = self.response_error_message(response)
            #Trigger 30000 limit according to looker's system
            if "30000" in response.text:
                self.override_prepared_request = True
                raise RetriableAPIError(msg, response)
            raise FatalAPIError(msg)

    def _request(
        self, prepared_request: requests.PreparedRequest, context: Optional[dict]
    ) -> requests.Response:
        if self.override_prepared_request:
            prepared_request = self.prepare_request(context, None)
        response = self.requests_session.send(prepared_request, timeout=self.timeout)
        if self._LOG_REQUEST_METRICS:
            extra_tags = {}
            if self._LOG_REQUEST_METRIC_URLS:
                extra_tags["url"] = prepared_request.path_url
            self._write_request_duration_log(
                endpoint=self.path,
                response=response,
                context=context,
                extra_tags=extra_tags,
            )
        #Reset it back so other requests are not affected    
        self.override_prepared_request = False
        self.validate_response(response)
        logging.debug("Response received successfully.")
        return response
